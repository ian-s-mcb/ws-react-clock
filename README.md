# ws-react-clock

A web app that displays the current time in real-time.

This project was guided the following two examples:
- [Heroku's example of hosting Node and react together][heroku-node-and-react]
- [Heroku's example of using Node with WebSockets][heroku-node-and-ws]

### [Demo][demo]

### Technologies used

- [React frontend][react]
- [create-react-app][create-react-app] - scaffolding for single page apps
- Node and [Express][express] backend
- [Heroku][heroku] - host for our backend
- [ws][ws] - an implmentation of a WebSocket server
- [react-websocket][react-websocket] - a web component that wraps the browser's built-in
  WebSocket client

[heroku-node-and-react]: https://github.com/mars/heroku-cra-node
[heroku-node-and-ws]: https://github.com/heroku-examples/node-ws-test
[demo]: https://ws-react-clock.herokuapp.com/
[react]: https://reactjs.org/
[create-react-app]: https://github.com/facebook/create-react-app
[express]: https://github.com/expressjs/express
[heroku]: https://www.heroku.com/
[ws]: https://github.com/websockets/ws
[react-websocket]: https://github.com/mehmetkose/react-websocket
