import React, { useState } from 'react'
import Websocket from 'react-websocket'

import WS_HOST from './WebSocketHelper'

function MyDate (props) {
  const [date, setDate] = useState('')

  return (
    <span>
      Date: <strong>{date}</strong>

      <Websocket
        url={WS_HOST}
        onMessage={setDate}
      />
    </span>
  )
}

export default MyDate
