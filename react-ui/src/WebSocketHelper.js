let WS_HOST = ''

if (process.env.NODE_ENV === 'development') {
  WS_HOST = 'ws://localhost:5000'
} else {
  WS_HOST = window.location.origin.replace(/^http/, 'ws')
}

export default WS_HOST
