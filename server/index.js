const express = require('express')
const { Server } = require('ws')
const path = require('path')

const PORT = process.env.PORT || 5000
const app = express()

// Priority serve any static files.
app.use(express.static(path.resolve(__dirname, '../react-ui/build')))

// Answer API requests.
app.get('/api', function (req, res) {
  res.set('Content-Type', 'application/json')
  res.send('{"message":"Hello from the custom server!"}')
})

// All remaining requests return the React app, so it can handle routing.
app.get('*', function (request, response) {
  response.sendFile(path.resolve(__dirname, '../react-ui/build', 'index.html'))
})

const server = app.listen(PORT, function () {
  console.error(`Listening on port ${PORT}`)
})

const wss = new Server({ server })

wss.on('connection', (ws) => {
  console.log('Client connected')
  ws.on('close', () => console.log('Client disconnected'))
})

setInterval(() => {
  wss.clients.forEach((client) => {
    client.send(new Date().toTimeString())
  })
}, 1000)
